package javascript;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ZipExtractor {
    private static final String TAG = "ZipExtractor";

    public static String getOutputDirName(Context context, String outputDirName) {
        File dir = context.getFilesDir();
        File outputDir = new File(dir.getAbsolutePath() + File.separator + outputDirName);
        return outputDir.getAbsolutePath();
    }

    public static void extractZip(Context context, String nameInAssets, String outputDirName, boolean forceUpdate) {
        File outputDir = new File(getOutputDirName(context, outputDirName));
        if (outputDir.exists()) {
            Log.d(TAG, "output directory already exists: " + outputDir.getAbsolutePath());
            if (forceUpdate) {
                Log.d(TAG, "forced update" + outputDir.getAbsolutePath());
                deleteRecursive(outputDir);
            } else {
                Log.d(TAG, "abort unzip" + outputDir.getAbsolutePath());
                return;
            }
        }
        if (!outputDir.mkdirs()) {
            Log.d(TAG, "unable to create output directory");
        }
        AssetManager am = context.getAssets();
        try {
            InputStream is = am.open(nameInAssets);
            ZipInputStream zin = new ZipInputStream(is);
            ZipEntry ze = null;
            while ((ze = zin.getNextEntry()) != null) {
                Log.v(TAG, "unzipping " + ze.getName());
                if (ze.isDirectory()) {
                    ensureDir(outputDir.getAbsolutePath() + File.separator + ze.getName());
                } else {
                    FileOutputStream fout = new FileOutputStream(outputDir.getAbsolutePath() + File.separator + ze.getName());
                    BufferedOutputStream bufout = new BufferedOutputStream(fout);
                    byte[] buffer = new byte[1024];
                    int read = 0;
                    while ((read = zin.read(buffer)) != -1) {
                        bufout.write(buffer, 0, read);
                    }
                    bufout.close();

                    zin.closeEntry();
                    fout.close();
                }
            }
            zin.close();
            Log.d(TAG, "unzipping complete. path: " + outputDir.getAbsolutePath());
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }
    }

    private static void deleteRecursive(File fileOrDir) {
        if (fileOrDir.isDirectory()) {
            for (File child : fileOrDir.listFiles()) {
                deleteRecursive(child);
            }
        }
        fileOrDir.delete();
    }

    private static void ensureDir(String dir) {
        File f = new File(dir);
        if (!f.exists()) {
            f.mkdirs();
        }
    }
}
