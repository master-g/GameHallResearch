package com.med.gamehall;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import javascript.AppActivity;
import javascript.ZipExtractor;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ZipExtractor.extractZip(this, "game1.zip", "game1", false);
        ZipExtractor.extractZip(this, "game2.zip", "game2", false);

        findViewById(R.id.id_btn_1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "click on button 1");
                Intent intent = new Intent(MainActivity.this, AppActivity.class);
                intent.putExtra("path", "game1");
                intent.putExtra("key", "12345");
                startActivity(intent);
            }
        });

        findViewById(R.id.id_btn_2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "click on button 2");
                Intent intent = new Intent(MainActivity.this, AppActivity.class);
                intent.putExtra("path", "game2");
                intent.putExtra("key", "12345");
                startActivity(intent);
            }
        });

        findViewById(R.id.id_btn_3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "click on button 3");
            }
        });
    }
}
