常见游戏大厅分析
====================

## 快手电丸与同桌游戏

快手电丸上的游戏都是基于 Cocos2d-x lua 实现 (跨平台, 但不支持浏览器, 必须通过 app 启动)  

同桌游戏上的游戏则是通过 Cocos2d-x javascript 实现 (跨平台, 支持浏览器, 可以脱离 app)  

快手电丸采用的是 app 内嵌定制化的 cocos2d-x 引擎, 加载不同的游戏文件达到游戏大厅的功能  

同桌游戏则直接用 webview 访问本地不同的 h5 游戏地址即可  

### 游戏性能对比

由于两款 app 上没有一模一样的游戏, 只能通过主观感进行对比  

结论: 快手电丸 > 同桌游戏  

推测: 由于快手电丸采用的是 cocos2d-x lua native 的解决方案, 其在效率和表现上优于采用 webview 的同桌游戏  

### 游戏下载速度对比

快手电丸下载游戏实际上就是下载 zip 包, 解压到本地目录就能够使用游戏引擎加载  

```plain
05-25 10:59:17.881 9598-15763/? V/SoGame: downloadSuccess downloadId=11
05-25 10:59:17.882 9598-15763/? V/SoGame: [perf]downloadSuccessGame gameId=21 starts
05-25 10:59:17.885 9598-15763/? V/SoGame: download game fileLength=2106562
05-25 10:59:18.285 9598-15763/? V/SoGame: download game unzip success
05-25 10:59:18.286 9598-15763/? V/SoGame: mark file gen not exist dir=/storage/emulated/0/SoGame/.gr/21_0.0.8
05-25 10:59:18.291 9598-15763/? V/SoGame: [perf]downloadSuccessGame gameId=21 ends in 408 ms
05-25 10:59:18.330 9598-15763/? V/SoGame: downloadSuccess end
```

两者游戏下载速度相差实际并不多, 并且都做了本地缓存  

但是由于同桌游戏采用的是 html5 的方案, 游戏需要依赖一些 js 的库  

这些库在 node_modules 目录下的文件繁多, 层级复杂, 这一部分文件在加载的时候消耗了大量的 io  

同桌游戏在没有网络的情况能够加载游戏, 而且加载速度与有网的情况下一致  

初步推断正是由于大量文件的 io 使得其在加载速度上的表现不如快手  

## 基于 Cocos2d-x 的游戏大厅实现

由于同桌游戏采用的是 h5 + webview 的解决方案, 在实现上相对快手而言要简单, 在此不再赘述  

### Cocos2d-x 3.x 版本 (lua)

以 Android 为例, 由于 cocos2d-x 引擎默认限制了游戏资源只能存放于 app 的 assets 目录下  

我们只能通过修改相关的文件加载逻辑来实现从指定的路径加载游戏  

简单来说, c++ 部分只需要修改以下几个文件:

```plain
Java_org_cocos2dx_lib_Cocos2dxHelper.cpp    // 修改游戏路径设置相关接口
CCFileUtils-android.cpp                     // 修改加载相对路径资源时的逻辑
AudioEngine-inl.cpp                         // 音频模块, 修改文件加载逻辑
```

在 java 层, 修改以下几个文件:

```plain
Cocos2dxActivity.java   // 修改销毁 activity 时同时销毁进程的逻辑
Cocos2dxHelper.java     // 修改设置资源管理器的代码, 增加设置游戏路径的逻辑
```

这样 Android 上的 cocos2d-x 引擎就能够从指定路径加载游戏了, 更详细的代码请参考[这个 repo](https://gitlab.com/master-g/GameHallResearch)  

其他平台如 iOS 上的修改与 Android 的类似.  

### CocosCreator 版本 (js)

CocosCreator 其实也是基于 Cocos2d-x 3.x 引擎的基础上的, 但采用的是 js 版本  

同样的, 通过修改 FileUtil-platform 和其他相关的几个模块, 就能够实现从任意路径加载游戏  

采用 js 版本的优点在于开发出来的游戏可以脱离 app 运行  

另外如果以 native 方式运行的话, 是直接驱动的 cocos2d-x 引擎, 相当于将脚本语言从 lua 换成了 js  

虽然js-binding 在运行效率上无法与 lua 相提并论, 但跨平台, 无需 app, 以及 js 生态的完备还是非常有吸引力的  

## 结论

快手电丸采用的是相对传统的 cocos2d-x lua 游戏方案, 在性能体验和团队开发经验上有优势, 缺点是游戏无法脱离 app 运行  

同桌游戏则采用了 cocos2d-x js html5 方案, 虽然游戏能够脱离 app 运行, 但是在性能和游戏体验上尚有一定差距  

不过同桌游戏可以参考快手电丸, 在 app 中部署修改过的 cocos2d-x js 引擎, 这样在 app 内是驱动 cocos2d-x 引擎, 效率能得到提升, 而且能够保留游戏能在 webview 里运行的优势  

|特性|快手电丸|同桌游戏|
|:---|:---|:---|
|性能|好|一般|
|游戏加载速度|快|相对较慢|
|垮平台性|一般|好|
