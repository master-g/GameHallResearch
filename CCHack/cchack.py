#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import time
import shutil


# -------------------------------------------------------------
# Help
# -------------------------------------------------------------

def printHelp():
    usage = """NAME:
    cchack - a patch for CocosCreator

USAGE:
    cchack [options] [creator_dir]

VERSION:
    0.0.1

COMMANDS:
    help, h  Shows a list of commands or help for one command

OPTIONS:
    --patch     apply patch to CocosCreator
    --restore   restore CocosCreator from restore folder
                you need to create a folder named 'restore'
                like 'restore/osx/...' or 'restore/win32/...'
    """
    print(usage)
    exit(1)


def str_time():
    return time.strftime('%Y%m%d_%p%H%M%S', time.localtime())


# -------------------------------------------------------------
# Backup
# -------------------------------------------------------------

def backup_osx(ccpath):
    files_to_backup = []
    for root, dirs, files in os.walk("patch"):
        for name in files:
            if name.startswith(".") or name == "cchacked":
                continue
            patch_file = os.path.join(root, name)
            comp = patch_file.split("osx/")
            comp = comp[len(comp) - 1]
            origin_file = os.path.join(ccpath, comp)
            if os.path.exists(origin_file):
                files_to_backup.append(origin_file)
    if len(files_to_backup) > 0:
        backup_dir = os.path.join("backup", str_time(), "osx", "Contents")
        for f in files_to_backup:
            backup_file = f.split("Contents/")[1]
            backup_file = os.path.join(backup_dir, backup_file)
            sub_dir = os.path.dirname(backup_file)
            if not os.path.exists(sub_dir):
                os.makedirs(sub_dir)
            print("backing up " + f)
            shutil.copyfile(f, backup_file)


def backup(ccpath):
    backup_osx(ccpath)


# -------------------------------------------------------------
# Replace
# -------------------------------------------------------------

def replace_osx(ccpath):
    for root, dirs, files in os.walk("patch"):
        for name in files:
            if name.startswith("."):
                continue
            patch_file = os.path.join(root, name)
            comp = patch_file.split("osx/")
            comp = comp[len(comp) - 1]
            origin_file = os.path.join(ccpath, comp)
            print("patching " + origin_file)
            shutil.copyfile(patch_file, origin_file)


def replace(ccpath):
    replace_osx(ccpath)


# -------------------------------------------------------------
# Patch
# -------------------------------------------------------------

def patch(ccpath):
    patch_mark_path = os.path.join(ccpath, "cchacked")
    if os.path.exists(patch_mark_path):
        with open(patch_mark_path) as f:
            for line in f:
                print("target already patched with cchack " + line)
                break
        f.close()
        exit(0)
    else:
        backup(ccpath)
        replace(ccpath)


# -------------------------------------------------------------
# Restore
# -------------------------------------------------------------

def restore_osx(ccpath):
    restore_dir = os.path.join("restore", "osx")
    for root, dirs, files in os.walk(restore_dir):
        for name in files:
            if name.startswith("."):
                continue
            restore_file = os.path.join(root, name)
            target_file = root.split("osx/")[1]
            target_file = os.path.join(ccpath, target_file, name)
            print("restoring " + target_file)
            shutil.copyfile(restore_file, target_file)
    patch_mark_path = os.path.join(ccpath, "cchacked")
    print("remove flag")
    os.unlink(patch_mark_path)


def restore(ccpath):
    patch_mark_path = os.path.join(ccpath, "cchacked")
    if not os.path.exists(patch_mark_path):
        print("target not patched, abort")
        exit(0)
    if not os.path.exists("restore"):
        print("restore folder not exists")
        exit(0)
    restore_osx(ccpath)


# -------------------------------------------------------------
# Entry
# -------------------------------------------------------------

def main(argv):
    if len(argv) < 2:
        printHelp()
    ccpath = argv[1]
    if not os.path.exists(ccpath):
        print(ccpath + " not exists")
        exit(0)

    if not os.path.exists("patch"):
        print("patch missing")
        exit(1)

    if argv[0] == "--patch" or argv[0] == "-p":
        patch(ccpath)
    elif argv[0] == "--restore" or argv[0] == "-r":
        restore(ccpath)


if __name__ == "__main__":
    main(sys.argv[1:])
