/****************************************************************************
Copyright (c) 2010-2012 cocos2d-x.org
Copyright (c) 2013-2014 Chukong Technologies Inc.

http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/

#include "platform/CCPlatformConfig.h"
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID

#include "platform/android/CCFileUtils-android.h"
#include "platform/CCCommon.h"
#include "platform/android/jni/JniHelper.h"
#include "platform/android/jni/Java_org_cocos2dx_lib_Cocos2dxHelper.h"
#include "base/ZipUtils.h"
#include <stdlib.h>
#include <sys/stat.h>

#define  LOG_TAG    "CCFileUtils-android.cpp"
#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)

using namespace std;

NS_CC_BEGIN

static std::string g_assetPath;

void FileUtilsAndroid::setAssetPath(const std::string &a)
{
  if (!a.empty()) {
    g_assetPath = std::string(a);
  }
}

std::string FileUtilsAndroid::getAssetFilePath(const std::string &filename)
{
    return g_assetPath + '/' + filename;
}

FileUtils* FileUtils::getInstance()
{
    if (s_sharedFileUtils == nullptr)
    {
        s_sharedFileUtils = new FileUtilsAndroid();
        if (!s_sharedFileUtils->init())
        {
          delete s_sharedFileUtils;
          s_sharedFileUtils = nullptr;
          CCLOG("ERROR: Could not init CCFileUtilsAndroid");
        }
    }
    return s_sharedFileUtils;
}

FileUtilsAndroid::FileUtilsAndroid()
{
}

FileUtilsAndroid::~FileUtilsAndroid()
{
}

bool FileUtilsAndroid::init()
{
    return FileUtils::init();
}

std::string FileUtilsAndroid::getNewFilename(const std::string &filename) const
{
    std::string newFileName = FileUtils::getNewFilename(filename);
    // ../xxx do not fix this path
    auto pos = newFileName.find("../");
    if (pos == std::string::npos || pos == 0)
    {
        return newFileName;
    }

    std::vector<std::string> v(3);
    v.resize(0);
    auto change = false;
    size_t size = newFileName.size();
    size_t idx = 0;
    bool noexit = true;
    while (noexit)
    {
        pos = newFileName.find('/', idx);
        std::string tmp;
        if (pos == std::string::npos)
        {
            tmp = newFileName.substr(idx, size - idx);
            noexit = false;
        }else
        {
            tmp = newFileName.substr(idx, pos - idx + 1);
        }
        auto t = v.size();
        if (t > 0 && v[t-1].compare("../") != 0 &&
             (tmp.compare("../") == 0 || tmp.compare("..") == 0))
        {
            v.pop_back();
            change = true;
        }else
        {
            v.push_back(tmp);
        }
        idx = pos + 1;
    }

    if (change)
    {
        newFileName.clear();
        for (auto &s : v)
        {
            newFileName.append(s);
        }
    }
    return newFileName;
}

bool FileUtilsAndroid::isFileExistInternal(const std::string& strFilePath) const
{
    if (strFilePath.empty())
    {
        return false;
    }

    bool bFound = false;
    std::string absolutePath = strFilePath;
    if (strFilePath[0] != '/') {
        absolutePath = g_assetPath + '/' + strFilePath;
    }

    FILE *fp = fopen(absolutePath.c_str(), "r");
    if (fp)
    {
      bFound = true;
      fclose(fp);
    }

    return bFound;
}

bool FileUtilsAndroid::isDirectoryExistInternal(const std::string& dirPath) const
{
    if (dirPath.empty())
    {
        return false;
    }

    std::string absolutePath = dirPath;
    const char *s = absolutePath.c_str();
    if (s[0] == '/') {
        // find absolute path in flash memory
        CCLOG("find in flash memory dirPath(%s)", s);
    } else {
        // find in asset fold
        CCLOG("find in apk dirPath(%s)", s);
        absolutePath = g_assetPath + '/' + dirPath;
        s = absolutePath.c_str();
    }

    struct stat st;
    if (stat(s, &st) == 0)
    {
      return S_ISDIR(st.st_mode);
    }
    
    return false;
}

bool FileUtilsAndroid::isAbsolutePath(const std::string& strPath) const
{
    // On Android, there are two situations for full path.
    // 1) Files in APK, e.g. assets/path/path/file.png
    // 2) Files not in APK, e.g. /data/data/org.cocos2dx.hellocpp/cache/path/path/file.png, or /sdcard/path/path/file.png.
    // So these two situations need to be checked on Android.
    if (strPath[0] == '/' || strPath.find(_defaultResRootPath) == 0)
    {
        return true;
    }
    return false;
}

FileUtils::Status FileUtilsAndroid::getContents(const std::string& filename, ResizableBuffer* buffer)
{
    if (filename.empty())
        return FileUtils::Status::NotExists;

    string fullPath = fullPathForFilename(filename);

    if (fullPath[0] == '/')
        return FileUtils::getContents(fullPath, buffer);

    string relativePath = g_assetPath + '/' + fullPath;
    return FileUtils::getContents(relativePath, buffer);
}

string FileUtilsAndroid::getWritablePath() const
{
    // Fix for Nexus 10 (Android 4.2 multi-user environment)
    // the path is retrieved through Java Context.getCacheDir() method
    string dir("");
    string tmp = JniHelper::callStaticStringMethod("org/cocos2dx/lib/Cocos2dxHelper", "getCocos2dxWritablePath");

    if (tmp.length() > 0)
    {
        dir.append(tmp).append("/");

        return dir;
    }
    else
    {
        return "";
    }
}

NS_CC_END

#endif // CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
