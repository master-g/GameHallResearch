#/usr/bin/env bash

pack_folder=$(basename $1)
proj_folder=$1/build/jsb-link
res_folder=${proj_folder}/res
src_folder=${proj_folder}/src
mkdir -p ${pack_folder}
cp -r ${res_folder} ${pack_folder}
cp -r ${src_folder} ${pack_folder}
cp ${proj_folder}/main.js ${pack_folder}
cp ${proj_folder}/project.json ${pack_folder}
cp -r /Applications/CocosCreator.app/Contents/Resources/cocos2d-x/cocos/scripting/js-bindings/script ${pack_folder}/script

